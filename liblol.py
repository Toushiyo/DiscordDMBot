from typing import Union
import requests
import json


class RiotWebAPI:
    def __init__(self, api_key: Union[str, None], verbose: bool = False):
        """Creates an instance of the RiotWebAPI class to gather information based on the summoner name supplied


        Args:
            api_key (Union[str, None]): Get this API Key from Riot Dev Portal
            verbose (bool, optional): Prints out crap when querying for summoner information. Defaults to False.
        """
        self.__API_KEY__ = api_key
        self._verbose = verbose

        if verbose:
            print(f"api_key=")

    def convert_json_to_dict(self, json_obj) -> dict:
        return json.loads(json_obj)

    def query_summoner_info(self, summoner_name: str) -> dict:

        #   Go get the summoners identifying information using the Riot summoner-v4 API
        summoner_id_json = self.query_summoner_id(summoner_name=summoner_name)
        summoner_id_dict = json.loads(summoner_id_json.text)

        if self._verbose:
            print(json.dumps(summoner_id_dict, indent=4))

        #   Go get the summoners stats using the Riot league-v4 API
        summoner_info_json = self.make_summoner_info_query(
            summoner_id=summoner_id_dict["id"]
        )
        summoner_info_dict = json.loads(summoner_info_json.text)

        if self._verbose:
            print(json.dumps(summoner_info_dict, indent=4))

        return summoner_info_dict[0]

    def query_summoner_id(self, summoner_name: str):
        """Makes a HTTPS request to fetch summoner id from the Riot Web API

        Args:
            encrypted_summoner_id (str): [description]

        """
        return requests.get(
            f"https://na1.api.riotgames.com/lol/summoner/v4/summoners/by-name/{summoner_name}?api_key={self.__API_KEY__}"
        )

    def make_summoner_info_query(self, summoner_id: str):
        """Makes a HTTPS request to fetch summoner stats from the Riot Web API

        Args:
            encrypted_summoner_id (str): [description]

        """
        return requests.get(
            f"https://na1.api.riotgames.com/lol/league/v4/entries/by-summoner/{summoner_id}?api_key={self.__API_KEY__}"
        )

    def __test__(self, summoner_name):
        summoner_info_dict = self.query_summoner_info(summoner_name=summoner_name)
        print(json.dumps(summoner_info_dict, sort_keys=True, indent=4))


if __name__ == "__main__":
    import platform
    import json
    from dotenv import load_dotenv
    from os import getenv

    environment_os = platform.system()

    if environment_os is "Windows":
        load_dotenv()

    RIOT_API_KEY = getenv("RIOT_API_KEY")

    riot_api = RiotWebAPI(api_key=RIOT_API_KEY, verbose=True)
    riot_api.__test__("SomeFNG")
